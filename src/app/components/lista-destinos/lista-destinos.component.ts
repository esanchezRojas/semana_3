import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  destinos: DestinoViaje[];
  all;

  constructor(private destinosApiClient: DestinosApiClient, private store: Store<AppState>) { 
    this.destinos = [];
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        const fav = data;
        if (data != null){
          this.updates.push('Se ha elegido a ' + data.nombre)
        }
      });
    store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
    this.store.select(state => state.destinos)
    .subscribe(data => {
      let d = data.favorito;
      if (d != null) {
        this.updates.push("Se eligió: " + d.nombre);
      }
    });
  }

  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }
  elegido(e: DestinoViaje){
    this.destinosApiClient.elegir(e);
  }



}
